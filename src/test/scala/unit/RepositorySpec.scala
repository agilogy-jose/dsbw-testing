package unit

import org.scalatest._
import museums.Repository


class RepositorySpec extends FlatSpec {
  val author = "Whatever"
  val title = "Some random title"

  val r = new Repository()

  "A repository" should "return the newly created object" in {

    val obj = r.create(author, title)

    assert(obj.author === author)

    assert(obj.name === title)
  }

  it should "retrieve the object by id" in {
    val obj = r.create(author, title)

    assert(r.find(obj.id).orNull === obj)
  }

  it should "delete objects" in {
    pending
  }
}
