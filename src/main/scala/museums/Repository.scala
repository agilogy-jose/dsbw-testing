package museums

import scala.collection.mutable

case class MuseumObject(id: Long, author: String, name: String)

class Repository {

  val instances = new mutable.HashMap[Long, MuseumObject]
  var lastId: Long = 0;

  def find(id: Long): Option[MuseumObject] = {
    instances.get(id)
  }

  def create(author: String, title: String): MuseumObject = {
    lastId += 1
    val instance = MuseumObject(lastId, author, title)
    instances.put(lastId, instance)
    instance
  }


}
